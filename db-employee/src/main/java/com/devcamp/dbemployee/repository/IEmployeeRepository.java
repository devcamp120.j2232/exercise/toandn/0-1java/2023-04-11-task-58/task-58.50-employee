package com.devcamp.dbemployee.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.dbemployee.model.CEmployee;

public interface IEmployeeRepository extends JpaRepository<CEmployee, Long> {

}
