package com.devcamp.dbemployee.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.dbemployee.model.CEmployee;
import com.devcamp.dbemployee.repository.IEmployeeRepository;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class CEmployeeController {
  @Autowired
  IEmployeeRepository iEmployeeRepository;

  @GetMapping("/employees")
  public ResponseEntity<List<CEmployee>> getDrinks() {
    try {
      List<CEmployee> employeeList = new ArrayList<CEmployee>();
      iEmployeeRepository.findAll().forEach(employeeList::add);
      if (employeeList.size() == 0) {
        return new ResponseEntity<>(employeeList, HttpStatus.NOT_FOUND);
      } else
        return new ResponseEntity<>(employeeList, HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
    }
  }
}
