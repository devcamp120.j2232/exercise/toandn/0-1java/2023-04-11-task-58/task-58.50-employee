package com.devcamp.dbemployee;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DbEmployeeApplication {

	public static void main(String[] args) {
		SpringApplication.run(DbEmployeeApplication.class, args);
	}

}
